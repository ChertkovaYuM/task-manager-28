package ru.tsc.chertkova.tm.api.repository;

import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnerRepository<Task> {

    @Nullable
    Task create(@Nullable String userId, @Nullable String name);

    @Nullable
    Task create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @Nullable
    List<Task> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

}
