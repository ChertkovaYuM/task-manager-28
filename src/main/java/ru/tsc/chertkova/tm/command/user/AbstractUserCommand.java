package ru.tsc.chertkova.tm.command.user;

import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.api.service.IAuthService;
import ru.tsc.chertkova.tm.api.service.IUserService;
import ru.tsc.chertkova.tm.command.AbstractCommand;

public abstract class AbstractUserCommand extends AbstractCommand {

    @Nullable
    public String getArgument() {
        return null;
    }

    @Nullable
    public IUserService getUserService() {
        return serviceLocator.getUserService();
    }

    @Nullable
    public IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

}
